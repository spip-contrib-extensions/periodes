<?php
/**
 * Déclarations des fonctions par Périodes
 *
 * @plugin     Périodes
 * @copyright  2019 - 2022
 * @author     Rainer Müller
 * @licence    GNU/GPL v3
 * @package    SPIP\Periodes\Fonctions
 */

if (!defined('_ECRIRE_INC_VERSION')) {
	return;
}

include_spip('filtres/inc_agenda_filtres');
